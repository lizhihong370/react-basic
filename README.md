## React Basic Knowledge

It is a little painful to setup a react development environment if you are not familiar with npm, nodejs, webpack, babel.

This is a simple template which has set up the most basic environment.

### Build & Run
After clone this git project, run command<br> 
`npm install --registry=https://registry.npm.taobao.org` <br>under the project root directory

or install [cnpm](https://npm.taobao.org/) which is a mirror repository maintained by Taobao, then run 
<br>`cnpm install` 
<br>under the project root directory

After add new js files or modify some file, you could build the project by run command 
<br>`npm run build`<br>
which will recompile the project and the output files(bundle.js and index.html) will under /build directory

The code is based on [the official tutorial ](https://reactjs.org/docs/hello-world.html)








