// var React = require("react");
import React, { Component } from "react";
import { Game } from "./Game";
require("../css/index.css");
import { Clock } from "./Clock";
import { ToggleButton } from "./ToggleButton";
import { Greeting } from "./Greeting";
import { LoginControl } from "./LoginControl";
import { Page } from "./Page";
import { NumberList } from "./NumberList";
import { FlavorForm } from "./FlavorForm";
import { NameForm } from "./NameForm";
import { Reservation } from "./Reservation";
import { Calculator } from "./LiftingState";

class App extends Component {
  render() {
    return (
      <div>
        {/* <Clock />
        <ToggleButton />
        <Greeting isLoggedIn={false} />
        <Greeting isLoggedIn={true} />
        <LoginControl />
        <Page /> */}

        {/* <NumberList /> */}

        <NameForm />
        <hr/>
        <FlavorForm />
        <br/>
        <Reservation />
        <br/>
        <Calculator />
      </div>
    );
  }
}

export { App };
